#!/usr/bin/env python
# Dependency libusb.dll in Kivy/MinGW/bin directory
import sys
if sys.version_info[0] != 3 or sys.version_info[1] < 4:
    print("This script requires Python version 3.4")
    sys.exit(1)

import kivy
kivy.require('1.9.0')

app_version = "1.0.0"
# for kivy top level applications
from kivy.app import App
# for kivy UI
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.settings import SettingsWithSidebar
from kivy.uix.settings import Settings
from kivy.uix.widget import Widget
# for logger
from kivy.logger import Logger
# for setting configuration
from settingsjson import settings_json
# for usb device
import usb.core
import usb.util
# for plotting
from kivy.garden.graph import Graph, MeshLinePlot
# for garbage collector
import gc
# for interval timer to support garbage collection
from kivy.clock import Clock
# for math function
from math import sin
# for multithreading
import threading
# for connection status property
from kivy.properties import BooleanProperty
# for property based event binding
from kivy.event import EventDispatcher
# for thread to sleep/idle
import time

class GarbageCollector():
    def __init__(self):
        Clock.schedule_interval(self.worker, 10)
    def worker(self, dt):
        collected = gc.collect()
        print("Garbage collector: collected %d objects." % (collected))

class CypressUSB(EventDispatcher):
    connected_flag = BooleanProperty(False)
    def __init__(self,vendor_id,product_id,stop):
        self.vendor_id = vendor_id
        self.product_id = product_id
        self.stop = stop
        self.t = threading.Thread(target=self.worker)
        self.t.start()
    def is_connected(self):
        self.dev = usb.core.find(idVendor=self.vendor_id,idProduct=self.product_id)
        if self.dev is None:
            self.connected_flag = False
            return False
        else:
            self.connected_flag = True
            return True
    def worker(self):
        while True:
            if (self.is_connected() == True):
                print("Connected")
            else:
                print("Disconnected")
            if self.stop.is_set():
                return
            time.sleep(1)

class HelpLayout(BoxLayout):
    label = Label(text = "")
    popup = Popup(title = "",
        content = Label(text = ""),
        size = (200,150),
        size_hint=(None, None),
        auto_dismiss=True)
    def update_status(self,status):
        label = self.ids['label_status']
        label.text = status
    def display_help(self):
        pass
    def display_about(self):
        self.label.text = "Version " + app_version + "."
        self.popup.title = "About"
        self.popup.content = self.label
        self.popup.open()

class MainWindow(BoxLayout):
    closed_event = threading.Event()
    plot = MeshLinePlot(color=[1, 0, 0, 1])
    cyusb = CypressUSB(0x221A,0x0100,closed_event)
    garbage_collector = GarbageCollector()
    def display_plot(self):
        self.plot.points = [(x, sin(x / 10.)) for x in range(0, 101)]
        graph = self.ids['graph_time_domain']
        graph.add_plot(self.plot)
    def update_connection(self, instance, value):
        help_layout = self.ids['layout_help']
        if value:
            help_layout.update_status("Connected")
        else:
            help_layout.update_status("Disconnected")

class MainApp(App):
    icon = "resources/icon.png"
    title = "Signal Monitor"
    def build(self):
        self.settings_cls = SettingsWithSidebar
        self.use_kivy_settings = True
        setting = self.config.get('example', 'boolexample')
        mainwindow = MainWindow()
        mainwindow.cyusb.bind(connected_flag=mainwindow.update_connection)
        return mainwindow
    def build_config(self, config):
        config.setdefaults('example', {
            'boolexample': True,
            'numericexample': 10,
            'optionsexample': 'option2',
            'stringexample': 'some_string',
            'pathexample': '/some/path'})
    def build_settings(self, settings):
        settings.add_json_panel('Panel Name',
            self.config,
            data = settings_json)
    def on_config_change(self, config, section, key, value):
        print(config,
            section,
            key,
            value)
    def on_start(self):
        Logger.info('App: I\'m alive!')
    def on_stop(self):
        Logger.critical('App: Aaaargh I\'m dying!')
        self.root.closed_event.set()

if __name__ == '__main__':
    MainApp().run()
